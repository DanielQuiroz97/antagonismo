library(rgl)
library(plot3D)
library(cluster)
library(ggfortify)
library(ggsci)
library(ggpubr)

# functions ----
read_msfinder <- function(msfinder) {
  read.csv(msfinder, sep = "\t") %>% 
    select(file = File.name,
           Rank1 = Structure.rank.1, 
           Ontology1 = Ontology, 
           Rank2 = Structure.rank.2, 
           Rank3 = Structure.rank.3) %>% 
    mutate(file = paste0(file, ".mat"))
}

# PCA ----

# Final features at least
final_index <- names(index_keep) %in% colnames(final_features)
index_keep <- index_keep[final_index]
index_blanco <- grep("Blanco",rownames(final_features))
final_features <- final_features[-index_blanco, index_keep]

# PCA index to discriminate between strains ----
index_strains <- c(3, 5, 7, 17, 19, 21, 31, 33, 35, 45, 48 )
# Index for multiple comparison
index_cumulo <- grep("ba", rownames(final_features))
index_qc <- grep("QC_D", rownames(final_features))
index_all <- c(index_strains, index_cumulo, index_blanco, index_qc)


# Subset
strains <- final_features[index_strains, ]
pca_strains <- prcomp(strains, scale. = T, center = T)

all_strains <- pca_strains$x[, 1:3] %>% as.data.frame()  %>% 
  mutate(Raw_names = rownames(.)) %>% 
  merge(metadata) %>% 
  mutate(Cepa = factor(Cepa)) %>% 
  mutate(Col = ifelse(Cepa == "Ps18", "red",
                      ifelse(Cepa == "Ps17", "blue", 
                             ifelse(Cepa == "Ps11", "green", NA))))


ggplot(all_strains, aes(PC1, PC3)) +
  geom_point(aes(color = Cepa,), size = 4)  
 



# PCA between cumulo and strains ----
feat_ST_and_cum <- final_features[index_all, ]
pca_all <- prcomp(log(feat_ST_and_cum + 1), center = T, scale. = T)

main_data <- pca_all$x[, 1:3] %>% as.data.frame()  %>% 
  mutate(Raw_names = rownames(.)) %>% 
  merge(metadata) %>% 
  mutate(Cepa = factor(Cepa)) %>% 
  mutate(Tipo = ifelse( grepl("ba", Raw_names) &  grepl("Agar", Raw_names),
                        "Agar Antagonism", 
                        ifelse( grepl("ba", Raw_names) &  grepl("cumulo", Raw_names),
                                "Deleyed Antagonism",
                                ifelse(grepl("Blanco", Raw_names), "Blank",
                                       "Axenic Culture")) )) %>% 
  mutate(Tipo = ifelse(grepl("QC", Raw_names), "QC", Tipo) ) %>% 
  mutate(Cepa = ifelse(is.na(Cepa), "QC", Cepa ))

ggplot(main_data, aes(PC1, PC2)) +
  geom_point(aes(color = Tipo), size = 3.5) +
  theme_pubclean() + ggsci::scale_color_jama() +
  xlab("PC1 (19.85%)") + ylab("PC2 (8.80%)")+
  ggrepel::geom_text_repel(aes(label = Code)) +
  #ylim(c(-20, 20)) + xlim(c(-40, 50)) +
  labs(color = "") +
  #stat_ellipse() +
  stat_ellipse(aes(color = Tipo), size = 1) +
  geom_vline(xintercept = 0, color = "black", alpha = 0.5) +
  geom_hline(yintercept = 0, color = "black", alpha = 0.5)

loadings <-  pca_all$rotation %>% data.frame() %>% 
  mutate(features = rownames(.)) %>% 
  select(features, PC1, PC2, PC3)

negative_ld <- loadings %>%
  filter_at(vars(starts_with("PC")), any_vars( . < -0.03 | . > 0.03) ) %>% 
  arrange(PC1) %>% 
  separate(features, into = c("mz", "rt"), sep = "_") %>% 
  mutate_if(is.character, as.numeric)

# source("clean_features.R")
match_mz2 <-  left_join(negative_ld, retention_times, by = "mz")
metabolites <- read_msfinder("spectra/Structure result-2071.txt")
identified <-  left_join(match_mz2, metabolites)
