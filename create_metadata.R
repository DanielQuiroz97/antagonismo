#### Load Libraries ####
library(tidyverse)
library(stringi)
library(magrittr)
# Get Row names
raw_names <- basename(mz_all)
# Create a tibble
metadata <- tibble(Raw_names = raw_names)

metadata %<>% 
  # Remove file extension
  mutate(sample_names = str_remove(string = Raw_names,
                                 pattern =  "_1.mzML.gz")) %>% 
  # Separate into categories
  separate(col = sample_names,
           into = c("Code", "Replica", "Fraction", "Tipo")) %>% 
  # Definde de bacterial strain
  mutate(Cepa = ifelse( grepl("Ps", Code),  Code, NA ) ) %>% 
  mutate(Cepa = str_remove(Cepa, "ba")) %>% 
  # Tipo de interaccion
  mutate(Interaccion = ifelse(grepl("ba$", Code), "Deleyed", "Direct"),
         Replica = factor(Replica) ) %>% 
  mutate(Unique_ID = paste(Code, Interaccion) ) %>% 
  mutate(Group = ifelse(Code == "QC", "QC", "Sample"))


#Read Injection List
injection <- read_csv("metadata/injection_list.csv")

# Join metadata and injection list
metadata %<>% left_join(injection) %>% arrange(Injection)

# Escribir la tabla de metadatos
#write.csv(metadata, "/metadata/metadatos.csv", row.names = F)