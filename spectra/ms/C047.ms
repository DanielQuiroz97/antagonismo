>compound C047
>parentmass 416.282119750977
>ionization [M+H]+

>ms1peaks
416.282119750977 100

>collision fill
416.282119750977 105675.653712119
189.119627702655 90458.9567814429
70.083442306775 57111.3612001072
356.743992641239 27924.6509852643

